import React, {createContext, useReducer} from 'react'
import ReactDOM from 'react-dom'
// import App from './App';
import reportWebVitals from './reportWebVitals'
import './sass/main.scss'
import Routes from './Routes'
import {reducer, initialState} from './reducers'
// import './index.css'
export const MainContext = createContext()

const Root = () => {
  // const initialState = {
  //   user: null,
  //   lastAdd: ['chuj','dupa','kurwa','cipa']
  // }
  const [state ,dispatch] = useReducer(reducer, initialState)
  return (
    <MainContext.Provider value={{state, dispatch}}>
      <Routes />
    </MainContext.Provider>
  )
}
ReactDOM.render( <Root />, document.getElementById('root'))
reportWebVitals()
