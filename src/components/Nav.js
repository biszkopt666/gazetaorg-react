import React, { useState, useContext } from 'react'
import { Link } from 'react-router-dom'
import LoginModal from '../modal/LoginModal'
import { MainContext } from '../index'

const Nav = () => {
  const { state, dispatch } = useContext(MainContext)
  const { user } = state
  const [openMenu, setOpenMenu] = useState(false)
  const [keyword, setKeyword] = useState('')
  const [openSearch, setOpenSearch] = useState(false)
  const [loginModal, setLoginModal] = useState(false)
  const handleLogin = event => {
    event.preventDefault()
    setLoginModal(true)
  }
  const handleLogout = event => {
    event.preventDefault()
    dispatch({ type: 'LOGOUT' })
    localStorage.removeItem('token')
  }
  const handleSearch = event => {
    if(!keyword) {
      event.preventDefault()
      setOpenSearch(!openSearch)
    } else {
      setKeyword('')
      setOpenSearch(false)
    }
  }
  return (
    <nav>
      <div id="logo">
        <Link to="/">GAZETA.ORG</Link>
      </div>
      <div
        id="hamburger"
        className={openMenu ? 'openmenu' : ''}
        onClick={() => setOpenMenu(!openMenu)}
      >
        <div className="hamburger-box">
          <div className="hamburger-inner"></div>
        </div>
      </div>
      <ul id="nav-main">
        <li>
          {' '}
          <Link to="/" data-router="menu" className="active">
            Aktualności
          </Link>{' '}
        </li>
        <li>
          {' '}
          <Link to="/kultura" data-router="menu" className="">
            Kultura
          </Link>{' '}
        </li>
        <li>
          {' '}
          <Link to="/sport" data-router="menu">
            Sport
          </Link>{' '}
        </li>
        <li>
          {' '}
          <Link to="/ogloszenia" data-router="menu" className="">
            Ogłoszenia
          </Link>{' '}
        </li>
        <li>
          {' '}
          <Link to="/kontakt" data-router="menu">
            Kontakt
          </Link>{' '}
        </li>
      </ul>
      <ul id="nav-right">
        <li id="search">
          <i className="fa fa-search" aria-hidden="true"></i>
          <input
            type="text"
            style={{
              width: openSearch ? '300px' : '0px',
              opacity: openSearch ? 1 : 0
            }}
            value={keyword}
            onChange={event => setKeyword(event.target.value)}
          />
          <Link
            to = {`/szukaj/${keyword}`}
            onClick={handleSearch}
            style={{ cursor: 'pointer' }}
          >
            Szukaj
          </Link>
        </li>
        <li id="login-group">
          {user ? (
            <React.Fragment>
              <Link id="profile-button" className="profile-button" to="/profil">
                <i className="fa fa-user" aria-hidden="true"></i>{' '}
                <span>{user.full_name.split(' ')[0]}</span>
              </Link>
              <a
                id="logout-button"
                className="logout-button"
                onClick={handleLogout}
                href="/logout"
              >
                {' '}
                <span>Wyloguj</span>{' '}
              </a>
            </React.Fragment>
          ) : (
            <a
              id="login-button"
              className="login-button"
              onClick={handleLogin}
              href="/login"
            >
              {' '}
              <i className="fa fa-user" aria-hidden="true"></i>{' '}
              <span>Login</span>{' '}
            </a>
          )}
        </li>
      </ul>
      <ul id="nav-mobile" style={{ display: openMenu ? 'block' : 'none' }}>
        <li id="mobile-search">
          <input type="text" />
          <a href="/szukaj">Szukaj</a>
        </li>
        <li>
          {' '}
          <Link to="/" data-router="menu" className="active">
            Aktualności
          </Link>{' '}
        </li>
        <li>
          {' '}
          <Link to="/kultura" data-router="menu" className="">
            Kultura
          </Link>{' '}
        </li>
        <li>
          {' '}
          <Link to="/sport" data-router="menu">
            Sport
          </Link>{' '}
        </li>
        <li>
          {' '}
          <Link to="/ogloszenia" data-router="menu" className="">
            Ogłoszenia
          </Link>{' '}
        </li>
        <li>
          {' '}
          <Link to="/kontakt" data-router="menu">
            Kontakt
          </Link>{' '}
        </li>
        <li id="mobile-login-group">
          <Link className="profile-button" to="/profil">
            <i className="fa fa-user" aria-hidden="true"></i>
            <span>Kamil</span>
          </Link>
          <a className="logout-button" href="/logout">
            {' '}
            <span>Wyloguj</span>
          </a>
        </li>
        <li id="mobile-menu-last">
          <div id="close-mobile-menu" onClick={() => setOpenMenu(false)}></div>
        </li>
      </ul>
      {loginModal && <LoginModal onClose={() => setLoginModal(false)} />}
    </nav>
  )
  // }
}
export default Nav
