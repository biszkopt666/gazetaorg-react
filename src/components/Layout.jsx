import React, {useState, useRef, useEffect} from 'react'
// import { useHistory } from 'react-router-dom'
// import Header from './Header'
import Nav from './Nav'
import Right from './Right'

const Layout = ({children, mainOffsetHeight}) => {
  const contentRef = useRef()
  const rightRef = useRef()
  // const history = useHistory()
  const [stick, setStick] = useState(false)
  const [rightCount, setRightCount] = useState(21)
  const [maxRightCount, setMaxRightCount] = useState(10)
  const handleScroll = () => {
    if (contentRef.current) {
      setStick(contentRef.current.getBoundingClientRect().top < -50)
    }
  }
  useEffect(() => {
    setRightCount(10)
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', () => handleScroll)
    }
  }, [])
  useEffect(()=>{
    // console.log(mainOffsetHeight);
    if (window.innerWidth >= 1050 && rightCount < maxRightCount) {
      if (rightRef.current.offsetHeight < mainOffsetHeight - 50) setRightCount(rightCount+1)
      else if(rightRef.current.offsetHeight > mainOffsetHeight) setRightCount(rightCount-1)
    }
  },[mainOffsetHeight, rightRef, rightCount, maxRightCount])
  return (
    <React.Fragment>
      <header className={stick?'stick':''}><Nav /></header>
      <div id="content" ref={contentRef}>
        <div id="top">
        </div>
        <div id="main">
          {children}
        </div>
        <div id="right" ref={rightRef}><Right count={rightCount} maxCount={c=>setMaxRightCount(c)} /></div>
      </div>
      <footer></footer>
    </React.Fragment>
  )
}
export default Layout
