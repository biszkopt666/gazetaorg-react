import React, {useEffect, useState} from 'react'
import { Link } from 'react-router-dom'

const Brick = ({article, deleteAction}) => {
  const {title, slug, cover, edit} = article
  const [imgHeight, setImgHeight] = useState(100)
  const titleRef = React.createRef()
  useEffect(()=>{
    setImgHeight((window.innerWidth>800?134:88)-titleRef.current.offsetHeight)
  },[titleRef])
  return(
    <div className="brick">
      {edit&&
        <div className="buttons">
          <Link to={`/artykul/${slug}/edytuj`} className="edit btn btn-primary btn-mini">Edytuj</Link>&nbsp;
          <span className="delete btn btn-danger btn-mini" onClick={deleteAction}>Usun</span>
        </div>
      }
      <Link to={`/artykul/${slug}`} className="cover-mini">
        <img className="image" src={cover} style={{height: `${imgHeight}px`}} alt={title} />
      </Link>
      <Link className="title" to={`/artykul/${slug}`}><h4 ref={titleRef}>{title}</h4></Link>
      <div className="author">Autor: Kamil</div>
    </div>
  )
}
export default Brick
