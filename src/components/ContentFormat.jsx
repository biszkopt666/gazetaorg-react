import React from 'react'

const ContentFormat = ({text}) => {
  const pList = []
  if (text) {
    text.split('\n').forEach((line) => {
      if (line.length>1) pList.push(line)
    })
  }
  return (
    <React.Fragment>
      {pList.map((line,i)=><p key={i}>{line}</p>)}
    </React.Fragment>
  )
}
export default ContentFormat
