import React, {useEffect, useContext} from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { MainContext } from '../index'
// import { getList } from '../api/right'



const Right = ({count, maxCount}) => {
  const getList = async() => {
    const advertisements = await axios.get('http://localhost:8000/api/adios/?limit=40')
    const articles = await axios.get('http://localhost:8000/api/articles/?limit=40')
    const elements = []
    advertisements.data.results.forEach((item) => {
      item.slug = '/ogloszenie/'+item.slug
      elements.push(item)
    })
    articles.data.results.forEach((item) => {
      item.slug = '/artykul/'+item.slug
      elements.push(item)
    })
    elements.sort((a, b) => -a.created.localeCompare(b.created))
    // console.log('ADVY z API',elements)
    return elements
  }
  const {state,dispatch} = useContext(MainContext)
  // const [data, setData] = useState([])
  useEffect(()=>{
    getList().then(data=>{
      dispatch({type:'POSTS',payload:data})
      maxCount(data.length)
    })
  },[dispatch])
  return(
    <div>
      <h4>Ostatnio dodane</h4>
      {state.rightElements.map((el,i)=>(
        <Link to={el.slug} style={{display:count>i?'block':'none'}} key={i}>
          <div> <span className="pubdate">{el.created.slice(11,16)}</span> - {(el.title.length>50)?el.title.slice(0,50)+'...':el.title} </div>
        </Link>
      ))}
    </div>
  )
}
Right.defaultProps = {
  count: 40
}
export default Right
