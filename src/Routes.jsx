import React, { useContext, useEffect } from 'react'
import { BrowserRouter, Switch, Route, useLocation } from 'react-router-dom'
import Articles from './screens/Articles'
import Article from './screens/Article'
import ArticleForm from './screens/ArticleForm'
import Advertisements from './screens/Advertisements'
import AdvertisementForm from './screens/AdvertisementForm'
import Search from './screens/Search'
import Contact from './screens/Contact'
import { MainContext } from './index'
import { userInfo } from './api/auth'

const Routes = () => {
  const { dispatch} = useContext(MainContext)
  useEffect(()=>{
    if(JSON.parse(localStorage.getItem('token'))) {
      (async () => {
        const user = await userInfo()
        dispatch({type:'USER',payload:user.data})
      })()
    }
  },[dispatch])
  const ScrollToTop = () => {
    const { pathname } = useLocation()
    useEffect(() => {
      window.scrollTo(0, 0)
    }, [pathname])
    return null
  }
  return (
    <BrowserRouter>
      <ScrollToTop />
      <Switch>
        <Route path='/' exact component={Articles} />
        <Route path='/artykul/nowy' exact component={ArticleForm} />
        <Route path='/artykul/:slug/edytuj' exact component={ArticleForm} />
        <Route path='/kontakt' exact component={Contact} />
        <Route path='/ogloszenie/nowe' exact component={AdvertisementForm} />
        <Route path='/ogloszenie/:slug' exact component={Advertisements} />
        <Route path='/ogloszenie/:slug/edytuj' exact component={AdvertisementForm} />
        <Route path='/ogloszenia' exact component={Advertisements} />
        <Route path='/szukaj/:slug' exact component={Search} />
        {/*
          <Route path='/aktualnosci' exact component={Articles} />
          <Route path='/kultura' exact component={Articles} />
          <Route path='/sport' exact component={Articles} />
          */}
        <Route path='/artykul/:slug' exact component={Article} />
        <Route path='/:category' exact component={Articles} />
      </Switch>
    </BrowserRouter>
  )
}
export default Routes
