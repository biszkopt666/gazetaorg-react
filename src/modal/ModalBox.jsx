import React, {useState, useEffect} from 'react'
import ReactDom from 'react-dom'

const ModalBox = ({isOpen, onClose, children, hasMain=true}) => {
  const [open, setOpen] = useState(false)
  const onEscKeyDown = event => {
    if (event.key === 'Escape') setOpen(false)
  }
  const handleClose = event => {
    event.preventDefault()
    document.body.style.overflow = 'unset'
    console.log('JEST CLOSE hanlde')
    window.removeEventListener('keydown', onEscKeyDown, false)
    setOpen(false)
    // onClose()
  }
  useEffect(()=>{
    if (isOpen ) {
      window.addEventListener('keydown', onEscKeyDown, false)
      document.body.style.overflow = 'hidden'
      setOpen(true)
    } else {
      window.removeEventListener('keydown', onEscKeyDown, false)
      document.body.style.overflow = 'unset'
      setOpen(false)
      // onClose()
    }
  },[isOpen])
  return ReactDom.createPortal(
    <div id="modal-box" className={`fade-${open?'in':'out'}`} onTransitionEnd={()=>open||onClose()} >
      <div className="background" onClick={handleClose} />
      {hasMain?( <div id="modal-main"> {children} </div> ):( <React.Fragment>{children}</React.Fragment>) }
      <span id="close" onClick={handleClose}>×</span>
    </div>,
    document.getElementById('modal-root')
  )
}
export default ModalBox
