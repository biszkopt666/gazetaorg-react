import React, { useState, useContext } from 'react'
import {MainContext} from '../index'
import { login, userInfo } from '../api/auth'
import ModalBox from './ModalBox'

const LoginModal = ({onClose}) => {
  const {dispatch} = useContext(MainContext)
  const [open, setOpen] = useState(true)
  const handleSubmit = async(event) => {
    event.preventDefault()
    const response = await login('kamil.biszkopt@wp.pl','dupa')
    if(response.status === 200) {
      const user = await userInfo()
      if(user.status === 200) {
        dispatch({type:'USER',payload:user.data})
      }
      setOpen(false)
    }
  }
  return (
    <ModalBox isOpen={open} onClose={onClose}>
      <form action="/login" method="post" id="login-form" onSubmit={handleSubmit}>
        <h3>Logowanie</h3>
        <p id="errorfield"></p>
        <input type="text" className="inputborder" placeholder="email" name="email"/>
        <input type="password" className="inputborder" placeholder="hasło" name="password"/>
        <button type="submit" className="btn btn-primary">Login</button>
        <p className="small">Nie posiadasz jeszcze konta? <a href="/rejestracja">Kliknij Tutaj!!!</a></p>
      </form>
    </ModalBox>
  )
}
export default LoginModal
