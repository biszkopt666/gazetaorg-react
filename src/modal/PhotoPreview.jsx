import React, { useState, useEffect } from 'react'
import ModalBox from './ModalBox'

const PhotoPreview = ({photoUrl, photoList, onClose}) => {
  const [url, setUrl] = useState(photoUrl)
  const [ready, setReady] = useState(false)
  const [touchStart, setTouchStart] = useState(null)
  const [photoFrameX, setPhotoFrameX] = useState('50%')
  const [index, setIndex] = useState(photoList.findIndex(item => item.original === photoUrl))
  const [dimensions, setDimensions] = useState({width:'100px',height:'100px'})
  const handleSize = () => {
    const bigImg = new Image()
    bigImg.src = url
    const {height, width} = bigImg
    let newWidth = 0
    let newHeight = 0
    if((width)>(window.innerWidth-60)) {
      newWidth = window.innerWidth-60
      newHeight = (window.innerWidth-60)/(width/height)
    } else {
      newWidth = width
      newHeight = height
    }
    if((newHeight)>(window.innerHeight-40)) {
      newWidth = (window.innerHeight-40)*(width/height)
      newHeight = window.innerHeight-40
    }
    if ((dimensions.height===newHeight)&&(dimensions.width===newWidth)) setReady(true)
    setDimensions({width:newWidth,height:newHeight})
  }
  useEffect(()=>{
    setReady(false)
    setUrl(photoList[index].original)
  },[index, photoList])
  const touchGalleryEnd = () => {
    if((Math.abs((window.innerWidth/2)-parseInt(photoFrameX))>(window.innerWidth/4))) {
      if((window.innerWidth/2)-parseInt(photoFrameX)>0) {
        if (index<(photoList.length-1)) setIndex(index+1)
      } else {
        if (index>0) setIndex(index-1)
      }
    }
    setPhotoFrameX('50%')
  }
  return(
    <ModalBox onClose={onClose} isOpen={true} hasMain={false}>
      <div id="photogallery-main" style={{left:photoFrameX}}>
        <img src={url} onLoad={handleSize} style={{...dimensions, opacity:ready?'1':'0'}}
          onTransitionEnd={()=>setReady(true)} alt={url}
          onTouchStart={(event)=>setTouchStart(event.changedTouches[0].clientX)}
          onTouchMove={(event)=>setPhotoFrameX((window.innerWidth/2)+(event.changedTouches[0].clientX-touchStart)+'px')}
          onTouchEnd={touchGalleryEnd}
        />
      </div>
      <div id="photogallery-previmg" style={{opacity:index>0?'1':'0'}} onClick={index>0?()=>setIndex(index-1):null}>
        <i className="fa fa-chevron-left" aria-hidden="true"></i>
      </div>
      <div id="photogallery-nextimg" style={{opacity:index<photoList.length-1?'1':'0'}} onClick={index<photoList.length-1?()=>setIndex(index+1):null}>
        <i className="fa fa-chevron-right" aria-hidden="true"></i>
      </div>
    </ModalBox>
  )
}
export default PhotoPreview
