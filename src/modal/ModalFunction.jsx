import React, {useState, useEffect} from 'react'
import ReactDom from 'react-dom'

const ModalFunction = ({isOpen, onClose, children, hasMain=true}) => {

  const [open, setOpen] = useState(false)

  const onEscKeyDown = event => {
    if (event.key === 'Escape') setOpen(false)
  }

  const handleClose = event => {
    event.preventDefault()
    setOpen(false)
  }

  useEffect(()=>{
    if (isOpen ) {
      window.addEventListener('keydown', onEscKeyDown, false)
      setOpen(true)
    } else {
      window.removeEventListener('keydown', onEscKeyDown, false)
      setOpen(false)
    }
  },[isOpen])

  return ReactDom.createPortal(
    <div id="modal-box" className={`fade-${open?'in':'out'}`} onTransitionEnd={()=>open||onClose()} >
      <div className="background" onClick={handleClose} />
      {hasMain?(
        <div id="modal-main">
          Modal suko
          {children}
        </div>
      ):(
        <React.Fragment>{children}</React.Fragment>)
      }
      <span id="close" onClick={handleClose}>×</span>
    </div>,
    document.getElementById('modal-root')
  )
}
export default ModalFunction
