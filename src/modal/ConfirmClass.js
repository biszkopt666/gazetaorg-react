import React from 'react'
// import React, { useEffect, useState } from 'react';
import ModalBox from './ModalBox'

class Confirm extends React.Component {
  constructor(props) {
    super(props)
    this.children = props.children
    this.show = props.show
  }
  // const callYes = () => {
  //   this.props.callback(true)
  //   this.props.close()
  // }
  render() {
    return (
      <ModalBox show={this.props.show} close={this.props.close}>
        <div>{this.children}</div>
        <button onClick={this.callYes}>Tak</button>
        <button onClick={this.props.close}>Nie</button>
      </ModalBox>
    )
  }
}
export default Confirm
