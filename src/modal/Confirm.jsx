import React, { useState } from 'react'

import ModalBox from './ModalBox'
const Confirm = ({ children, onClose, confirmAction }) => {
  const [open, setOpen] = useState(true)
  const confirm = async () => {
    await setOpen(false)
    confirmAction()
  }
  return (
    <ModalBox isOpen={open} onClose={onClose}>
      <h3>{children}</h3>
      <button className="half-width btn btn-danger" onClick={confirm}>
        Tak
      </button>
      <button
        className="half-width btn btn-primary"
        onClick={() => setOpen(false)}
      >
        Nie
      </button>
    </ModalBox>
  )
}
export default Confirm
