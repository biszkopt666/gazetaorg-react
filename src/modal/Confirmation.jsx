import React, { useState } from 'react'

import ModalFunction from './ModalFunction'

const Confirmation = ({ children, onClose, confirmed }) => {
  const [open, setOpen] = useState(true)
  const confirm = () => {
    setOpen(false)
    confirmed(true)
  }
  return (
    <ModalFunction isOpen={open} onClose={onClose}>
      {children}
      <button onClick={confirm}>Tak</button>
      <button onClick={() => setOpen(false)}>Nie</button>
    </ModalFunction>
  )
}
export default Confirmation
