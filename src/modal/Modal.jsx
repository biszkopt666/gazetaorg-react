import React from 'react'
import ReactDom from 'react-dom'
// importort PropTypes from "prop-types";

// const modalRoot = document.getElementById("modal-root");

class Modal extends React.Component {
  // static propTypes = {
  // onClose: PropTypes.func.isRequired,
  // isOpen: PropTypes.bool.isRequired,
  // };

  // state = { fadeType: null };

  componentDidMount() {
    window.addEventListener('keydown', this.onEscKeyDown, false)
    setTimeout(() => this.setState({ fadeType: 'in' }), 0)
    // this.setState({fadeType:'in'})
  }

  componentDidUpdate(prevProps) {
    if (!this.props.isOpen && prevProps.isOpen) this.setState({ fadeType: 'out' })
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.onEscKeyDown, false)
  }

  transitionEnd(event) {
    if (event.propertyName !== 'opacity' || this.state.fadeType === 'in') return
    if (this.state.fadeType === 'out') this.props.onClose()
    console.log('event.propertyName',event.propertyName)
  }

  onEscKeyDown(event) {
    if (event.key !== 'Escape') return
    this.setState({ fadeType: 'out' })
  }

  handleClose(event) {
    event.preventDefault()
    this.setState({ fadeType: 'out' })
  }

  render() {
    return ReactDom.createPortal(
      <div id="modal-box" className={`fade-${ this.state.fadeType }`} onTransitionEnd={this.transitionEnd} >
        <div id="modal-main">
          Modal suko
          {this.props.children}
        </div>
        <div className="background" onMouseDown={this.handleClose} />
        <span id="close" onClick={this.handleClose}>×</span>
      </div>,
      document.getElementById('modal-root')
    )
  }
}

export default Modal
