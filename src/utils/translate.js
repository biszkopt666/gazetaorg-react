export const translateCategory = (category) => {
  switch(category){
  case 'aktualnosci':
    return 'news'
  case 'kultura':
    return 'culture'
  case 'sport':
    return 'sport'
  case 'news':
    return 'aktualnosci'
  case 'culture':
    return 'kultura'
  default: return null
  }
}
