import React, { useState, useEffect, createRef } from 'react'
import Layout from '../components/Layout'
import { useParams } from 'react-router-dom'
import axios from 'axios'

const TitleConvert = ({ text, keyword }) => {
  const pos = text.toLowerCase().indexOf(keyword)
  console.log('Position', pos)
  if (pos > 0) {
    return (
      <h3>
        {text.slice(0, pos)}
        <span className="search-fragment">
          {text.slice(pos, pos + keyword.length)}
        </span>
        {text.slice(pos + keyword.length, text.length)}
      </h3>
    )
  } else {
    return <h3>{text}</h3>
  }
}
const ContentConvert = ({ text, keyword }) => {
  const pos = text.toLowerCase().indexOf(keyword)
  let start = 0
  let end = text.length
  if (pos > 100) start = pos - 100
  if (text.length - pos - keyword.length > 100) end = pos + keyword.length + 100
  if (pos >= 0) {
    return (
      <p>
        ...{text.slice(start, pos)}
        <span className="search-fragment">
          {text.slice(pos, pos + keyword.length)}
        </span>
        {text.slice(pos + keyword.length, end)}
      </p>
    )
  } else {
    if (text.length > 200) {
      return (
        <p>
          ...
          {text.slice(
            parseInt(text.length / 2 - 100),
            parseInt(text.length / 2 + 100)
          )}
          ...
        </p>
      )
    } else {
      return <p>{text}</p>
    }
  }
}
const SearchResult = ({ title, content, slug, keyword }) => {
  return (
    <div className="search-result">
      <a href={`/artykul/${slug}`}>
        <TitleConvert text={title} keyword={keyword} />
      </a>
      <ContentConvert text={content} keyword={keyword} />
    </div>
  )
}

const Search = () => {
  const [offsetHeight, setOffsetHeight] = useState(0)
  const [data, setData] = useState({})
  const mainRef = createRef()
  const { slug } = useParams()
  useEffect(() => {
    setOffsetHeight(mainRef.current.offsetHeight)
  }, [mainRef])
  useEffect(() => {
    axios
      .get(`/api/articles/?search=${slug}`)
      .then(res => {
        if (res.status === 200) {
          setData(res.data)
        }
      })
      .catch(err => {
        console.log('error kurwa', err.response)
      })
  }, [slug])
  console.log(data)
  return (
    <Layout mainOffsetHeight={offsetHeight}>
      <div ref={mainRef} className="container">
        <h3>Wyniki wyszukiwania dla &quot;{slug}&quot;</h3>
        {data.results &&
          data.results.map(result => (
            <SearchResult
              key={result.id}
              title={result.title}
              keyword={slug}
              slug={result.slug}
              content={result.content}
            />
          ))}
      </div>
    </Layout>
  )
}

export default Search
