import React, { useEffect, useState, useContext, createRef } from 'react'
import Layout from '../components/Layout'
import { MainContext } from '../index'
import axios from 'axios'
import { useParams } from 'react-router-dom'

const AdvertisementForm = () => {
  // const [offsetHeight,setOffsetHeight] = useState(0)
  // const [data,setData] = useState([])
  // const [advertisement,setAdvertisement] = useState({})
  const [advertisement,setAdvertisement] = useState({
    title:'',
    content:'',
    category:'',
    price:0,
  })
  const [categories, setCategories] = useState([])
  const { slug } = useParams()
  const {state} = useContext(MainContext)
  // const [searchUrl, setSearchUrl] = useState('')
  const mainRef = createRef()
  const {user} = state
  console.log(user)
  const handleSubmit = event => {
    event.preventDefault()
    console.log('SUBMIT')
  }
  useEffect(()=>{
    console.log('SLUG',slug)
    if(slug) {
      axios.get(`http://localhost:8000/api/adios/${slug}`)
        .then(res=>{
          if(res.status === 200) {
            setAdvertisement(res.data)
          }
        }).catch((err) => {
          console.log('error kurwa', err.response)
        })
    }
    axios.get('http://localhost:8000/api/adios/category/').then(c=>{
      setCategories(c.data)
      // console.log("KATEGORIE",categories);
    })
  },[slug])
  return (
    <Layout >
      <div ref={mainRef}>
        <div className="go-back">
          <a href="//ogloszenie/citroen-c4-cactus15"> <i className="fa fa-chevron-left"></i><i className="fa fa-chevron-left"></i> </a>
        </div>
        <form action="/advertisement/new" method="post" className="embed-form" id="advertisement-form" onSubmit={handleSubmit}>
          <input type="text" className="inputborder" name="title" value={advertisement.title}
            onChange={event=>setAdvertisement({...advertisement, title:event.target.value})} />
          <textarea name="content" className="inputborder" value={advertisement.content}
            onChange={event=>setAdvertisement({...advertisement, content:event.target.value})} />
          <select name="category" value={advertisement.category}
            onChange={event=>setAdvertisement({...advertisement, category:event.target.value})}>
            {categories&&categories.map(category=>(
              <option key={category.slug} value={category.slug}>{category.name}</option>
            ))}
            <option value="add" id="add-category">+ Dodaj</option>
          </select>
          <input type="number" className="inputborder" name="price" placeholder="cena" value={advertisement.price}
            onChange={event=>setAdvertisement({...advertisement, price:event.target.value})} />
          <button type="submit" className="btn btn-primary adv-submit">Gotowe</button>
        </form>
      </div>
      {/*
          {JSON.stringify(data)}
        */}
    </Layout>
  )
}
export default AdvertisementForm
