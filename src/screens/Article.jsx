import React, {useState, useEffect, useContext, createRef} from 'react'
import { MainContext } from '../index'
import Layout from '../components/Layout'
// import ModalBox from '../modal/ModalBox'
import PhotoPreview from '../modal/PhotoPreview'
import Confirm from '../modal/Confirm'
import { useParams, Link } from 'react-router-dom'
import axios from 'axios'

const ContentFormat = ({text}) => {
  const pList = []
  if (text) {
    text.split('\n').forEach((line) => {
      if (line.length>1) pList.push(line)
    })
  }
  return (
    <React.Fragment>
      {pList.map((line,i)=><p key={i}>{line}</p>)}
    </React.Fragment>
  )
}
// const PhotoDeleteConfirm = ({photoId, onClose}) => {
//   return (
//     <Confirm onClose={onClose}>Chcesz?</Confirm>
//   )
// }
const Article = () => {
  const [data,setData] = useState('')
  const [photos,setPhotos] = useState([])
  const [preview,setPreview] = useState('')
  const [removePhoto, setRemovePhoto] = useState(null)
  const [uploadPhotos, setUploadPhotos] = useState([])
  const [removeArticle, setRemoveArticle] = useState(null)
  const [offsetHeight,setOffsetHeight] = useState(0)
  const mainRef = createRef()
  const { slug } = useParams()
  const {state} = useContext(MainContext)
  const {user} = state
  useEffect(()=>{
    axios.get(`http://localhost:8000/api/articles/${slug}`)
      .then(res=>{
        if(res.status === 200) {
          setData(res.data)
          axios.get(`http://localhost:8000/api/photos/?category=${slug}`).then(phRes=>{
            setPhotos(phRes.data)
          })
        }
      }).catch((err) => {
        console.log('error kurwa', err.response)
      })
  },[slug])
  useEffect(()=>{
    setOffsetHeight(mainRef.current.offsetHeight)
    // console.log(mainRef.current.offsetHeight);
  },[mainRef])
  const photoRemove = () => {
    const photoId = removePhoto
    setRemovePhoto(null)
    setPhotos(photos.filter(photo => photo.id !== photoId))
    console.log('Wypierdalam fotku',photoId)
  }
  const articleRemove = () => {
    const articleSlug = removeArticle
    setRemoveArticle(null)
    // setPhotos(photos.filter(photo => photo.id !== photoId))
    console.log('Wypierdalam artykul',removeArticle)
    console.log('Wypierdalam artykul',articleSlug)
  }
  const handleUploadPhotos = () => {
    const newPhotos = []
    uploadPhotos.forEach((photo,i)=>{
      const data = new FormData()
      data.append('category', slug)
      data.append('original', photo)
      const headers = user?{ 'Content-Type': 'application/json', 'Authorization':`Token ${user.token}` }:{ 'Content-Type': 'application/json'}
      axios.post('http://localhost:8000/api/photos/', data, {headers})
        .then((res) => {
          newPhotos.push(res.data)
        })
        .catch((err) => {console.log('Byk',err)})
      if(uploadPhotos.length===i+1) {
        setPhotos([...photos, ...newPhotos])
        setUploadPhotos([])
      }
    })
  }
  return (
    <Layout mainOffsetHeight={offsetHeight}>
      <div ref={mainRef}>
        <div className="go-back">
          <Link to={`/${data.category}`}> <i className="fa fa-chevron-left" aria-hidden="true"></i><i className="fa fa-chevron-left" aria-hidden="true"></i> </Link>
        </div>
        <div className="article">
          <div className="buttons">
            <Link to={`/artykul/${slug}/edytuj`} className="edit btn btn-primary btn-mini">Edytuj</Link>&nbsp;
            <span onClick={()=>setRemoveArticle(data.slug)} className="delete btn btn-danger btn-mini" data-category="aktualnosci">Usun</span>
          </div>
          <h3 id="title">{data.title}</h3>
          <img className="cover" src={data.cover} alt={data.title} />
          <div className="main-content"><ContentFormat text={data.content} /></div>
          <div className="photo-thumbnails" id="article-thumbnails">
            <div id="photos-confirm-alert"
              style={{opacity:uploadPhotos.length>0?'1':'0', width:'100%', textAlign:'center', fontWeight:'bold',color:'#da0000'}}>Potwierdź aby przesłać</div>
            {/*
                */}
            <div className="flagstone">
              <div className="photos-action" style={{opacity:'1',display:uploadPhotos.length>0?'block':'none',cursor:'pointer'}} onClick={handleUploadPhotos}>
                <i className="fa fa-check" aria-hidden="true" style={{color:'#00ee00'}}></i>
              </div>
              <label htmlFor="add-photos" className="image-upload thumbnail" style={{padding:'0px',textAlign:'center'}}>
                <i className="fas fa-upload" style={{fontSize:'40px',marginTop:'20px'}} aria-hidden="true"></i>
              </label>
              <input type="file" name="add-photos" style={{display:'none'}} id="add-photos" multiple onChange={event=>setUploadPhotos([...event.target.files])} />
            </div>

            {photos.map(photo=>(
              <div key={photo.id} className="flagstone">
                <div className="photos-action" onClick={()=>setRemovePhoto(photo.id)}>
                  <i className="fa fa-times" aria-hidden="true" style={{color: '#dc0000',cursor:'pointer'}}></i>
                </div>
                <img src={photo.mini} className="thumbnail" data-big={photo.original} onClick={()=>setPreview(photo.original)} alt={data.title} />
              </div>
            ))}
            {uploadPhotos.map((photo, i)=>(
              <div className="flagstone new-photo" key={i}>
                <div className="photos-action" onClick={()=>setUploadPhotos(uploadPhotos.filter(p=> photo!== p))}>
                  <i className="fa fa-times" aria-hidden="true" style={{color:'#dc0000',cursor:'pointer'}}></i>
                </div>
                <img src={URL.createObjectURL(photo)} className="thumbnail" alt={photo.name} />
              </div>
            ))}
            {preview&&<PhotoPreview onClose={()=>setPreview(null)} photoUrl={preview} photoList={photos} />}
            {removePhoto&&
                  <Confirm onClose={()=>setRemovePhoto(null)} confirmAction={photoRemove} isOpen={removePhoto}>
                    Czy na pewno chcesz usunąć to zdjęcie?
                  </Confirm>
            }
            {removeArticle&&
                  <Confirm onClose={()=>setRemoveArticle(null)} confirmAction={articleRemove} isOpen={removeArticle} >
                    Czy na pewno chcesz usunąć ten artykul?
                  </Confirm>
            }
          </div>
        </div>
      </div>
    </Layout>
  )
}
export default Article
