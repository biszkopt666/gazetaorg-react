import React, {useState, useEffect, useContext} from 'react'
import { MainContext } from '../index'
import Layout from '../components/Layout'
// import ModalBox from '../modal/ModalBox'
// import PhotoPreview from '../modal/PhotoPreview'
import { translateCategory } from '../utils/translate'
// import Confirm from '../modal/Confirm'
import { useParams, Link } from 'react-router-dom'
import axios from 'axios'

const Article = () => {
  const { slug } = useParams()
  // const [data,setData] = useState('')
  const [title, setTitle] = useState('')
  const [category, setCategory] = useState('')
  const [content, setContent] = useState('')
  const [errors, setErrors] = useState({})
  // const [photos,setPhotos] = useState([])
  // const [preview,setPreview] = useState('')
  // const [removePhoto, setRemovePhoto] = useState(null)
  // const [uploadPhotos, setUploadPhotos] = useState([])
  // const [removeArticle, setRemoveArticle] = useState(null)
  const {state} = useContext(MainContext)
  const {user} = state
  useEffect(()=>{
    if (slug && slug !== 'nowy') {
      console.log('edit')
      axios.get(`http://localhost:8000/api/articles/${slug}`)
        .then(res=>{
          if(res.status === 200) {
          // setData(res.data)
            setTitle(res.data.title)
            setContent(res.data.content)
            setCategory(res.data.category)
          }
        }).catch((err) => {
          console.log('error kurwa', err.response)
        })
    }
  },[slug])
  const hanldeSubmit = (event) => {
    event.preventDefault()
    console.log('submit')
    console.log(title)
    console.log(category)
    const body = new FormData()
    body.append('title',title)
    body.append('content',content)
    body.append('category', category.slice(0,1))
    if (slug) {
      console.log('mamy slug')
      axios.put(`http://localhost:8000/api/articles/${slug}`, body, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization':`Token ${user.token}`
        }
      })
    } else {
      console.log('nie mamy sluga')
      console.log('Kategoria', category)
      axios.post('http://localhost:8000/api/articles/', body, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization':`Token ${user.token}`
        }
      }).then(res=>{
        console.log(res)
        console.log(res.data)
      }).catch((err) => {
        console.log('byk',err)
        console.log('byk',err.response.status)
        console.log('byk',err.response.data)
        if(err.response.status === 400) setErrors(err.response.data)
      })
    }
  }
  return (
    <Layout>
      <div className="go-back">
        <Link to={`/${translateCategory(category)}`}> <i className="fa fa-chevron-left" aria-hidden="true"></i><i className="fa fa-chevron-left" aria-hidden="true"></i> </Link>
      </div>
      <form action="/article/new" method="post" className="embed-form" onSubmit={hanldeSubmit}>
        <input type="text" className={`inputborder ${errors.title&&'warning'}`} name="title" value={title} onChange={event=>setTitle(event.target.value)} />
        <textarea name="content" className={`inputborder ${errors.content&&'warning'}`} value={content} onChange={event=>setContent(event.target.value)} />
        <label htmlFor="cover" className="image-upload"> <i className="fas fa-upload" aria-hidden="true"></i> Dodaj okładkę</label>
        <input type="file" name="cover" />
        <div style={{float:'right', marginRight: '-21px'}} >
          <select value={(category).slice(0,1)} onChange={event=>setCategory(event.target.value)}>
            <option>Kategoria</option>
            <option value="n">Aktualności</option>
            <option value="c">Kultura</option>
            <option value="s">Sport</option>
          </select>
          <button type="submit" className="btn btn-primary">Gotowe</button>
        </div>
      </form>
    </Layout>
  )
}
export default Article
