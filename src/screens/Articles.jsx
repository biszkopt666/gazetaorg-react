import React, {useState, useEffect, useContext, createRef} from 'react'
import Layout from '../components/Layout'
import Brick from '../components/Brick'
import axios from 'axios'
import { MainContext } from '../index'
import Confirm from '../modal/Confirm'
import { useParams, Link } from 'react-router-dom'
import {translateCategory} from '../utils/translate'

const Articles = ({ history }) => {
  const { category } = useParams()
  const [data,setData] = useState([])
  const [offsetHeight,setOffsetHeight] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)
  const [removeArticle, setRemoveArticle] = useState(null)
  const {state} = useContext(MainContext)
  const {user} = state
  const mainRef = createRef()
  const articleRemove = () => {
    const articleSlug = removeArticle
    setRemoveArticle(null)
    // setPhotos(photos.filter(photo => photo.id !== photoId))
    console.log('Wypierdalam artykul',removeArticle)
    console.log('Wypierdalam artykul',articleSlug)
  }
  useEffect(()=>{
    history.push({ search: currentPage===1?'':`?strona=${currentPage}` })
    const getPage = () => {
      const searchParams = new URLSearchParams(history.location.search)
      setCurrentPage(searchParams.get('strona')||1)
      // const limit = user?2:3;
      const limit = user?59:60
      const offset = (currentPage-1)*limit
      return `&limit=${limit}&offset=${offset}`
    }
    const headers = user?{ 'Content-Type': 'application/json', 'Authorization':`Token ${user.token}` }:{ 'Content-Type': 'application/json'}
    axios.get(`/api/articles/?category=${translateCategory(category||'aktualnosci').slice(0,1)}${getPage()}`,{headers})
      .then(res=>{
        if(res.status === 200) {
          setData(res.data)
        }
      }).catch((err) => {
        console.log('error kurwa', err.response)
      })
  },[category,history.location.search, user, currentPage])
  useEffect(()=>{
    setOffsetHeight(mainRef.current.offsetHeight)
  },[mainRef])
  return (
    <Layout mainOffsetHeight={offsetHeight}>
      <div id="article-bricks" ref={mainRef}>
        <div className="brick add-brick">
          <div className="image" style={{display: 'table'}}>

            <Link to="/artykul/nowy" className="edit"><i className="fas fa-plus" aria-hidden="true"></i></Link>
          </div>
        </div>
        {/*
        {JSON.stringify(data)}
        {JSON.stringify(removeArticle)}
        */}
        {data.results && data.results.map(article => (
          <Brick key={article.slug} article={article} deleteAction={()=>setRemoveArticle(article.slug)} />
        ))}
      </div>
      <div id="paginator">
        <div style={{float:'left',display:data.previous?'table':'none',cursor:'pointer'}} onClick={()=>setCurrentPage(currentPage-1)}>
          <i className="fa fa-chevron-left" aria-hidden="true"></i>
          <i className="fa fa-chevron-left" aria-hidden="true"></i>
        </div>
        <div style={{float:'right',display:data.next?'table':'none',marginRight:'5px',cursor:'pointer'}} onClick={()=>setCurrentPage(parseInt(currentPage)+1)} >
          <i className="fa fa-chevron-right" aria-hidden="true"></i>
          <i className="fa fa-chevron-right" aria-hidden="true"></i>
        </div>
      </div>
      {removeArticle&&
        <Confirm onClose={()=>setRemoveArticle(null)} confirmAction={articleRemove} isOpen={removeArticle} >
          Czy na pewno chcesz usunąć ten artykul?
        </Confirm>
      }
    </Layout>
  )
}
export default Articles
