import React, { useEffect, useState, useContext, createRef } from 'react'
import Layout from '../components/Layout'
import ContentFormat from '../components/ContentFormat'
import { MainContext } from '../index'
import axios from 'axios'
import { useParams, Link } from 'react-router-dom'

const Advertisements = ({history}) => {
  const [offsetHeight,setOffsetHeight] = useState(0)
  const [data,setData] = useState([])
  const [advertisement,setAdvertisement] = useState({})
  const [categories, setCategories] = useState([])
  const { slug } = useParams()
  const {state} = useContext(MainContext)
  const searchParams = new URLSearchParams(history.location.search)
  const [currentPage, setCurrentPage] = useState(searchParams.get('strona')||1)
  const [searchValues, setSearchValues] = useState({
    item: searchParams.get('item')||'',
    price: searchParams.get('price')||'',
    category: searchParams.get('category')||'',
  })
  const [searchUrl, setSearchUrl] = useState('')
  const mainRef = createRef()
  const {user} = state
  useEffect(()=>{
    setCurrentPage(searchParams.get('strona')||1)
    setSearchValues({
      item: searchParams.get('item')||'',
      price: searchParams.get('price')||'',
      category: searchParams.get('category')||'',
    })
    const getPage = () => {
      const limit = 20
      const offset = (currentPage-1)*limit
      return `?limit=${limit}&offset=${offset}`
    }
    axios.get(`/api/adios/${getPage()}${history.location.search&&`&${searchUrl}`}`)
      .then(res=>{
        if(res.status === 200) {
          setData(res.data)
        }
      }).catch((err) => {
        console.log('error kurwa', err.response)
      })
    if (slug) {
      axios.get(`/api/adios/${slug}`)
        .then(res=>{
          if(res.status === 200) {
            setAdvertisement(res.data)
          }
        }).catch((err) => {
          console.log('error kurwa', err.response)
        })
    }
  },[slug,history.location, user, currentPage])
  useEffect(()=>{
    setOffsetHeight(mainRef.current.offsetHeight)
  },[mainRef])
  useEffect(()=>{
    setSearchUrl(
      new URLSearchParams({
        ...(searchValues.item && {item: searchValues.item.toLowerCase()}),
        ...(searchValues.category && {category: searchValues.category.toLowerCase()}),
        ...(searchValues.price && {price: searchValues.price}),
      }).toString()
    )
  },[searchValues])
  useEffect(()=>{
    axios.get('http://localhost:8000/api/adios/category/').then(c=>{
      setCategories(c.data)
      console.log('KATEGORIE',categories)
    })
  },[])
  return (
    <Layout mainOffsetHeight={offsetHeight}>
      <div ref={mainRef} id="advertisements">
        <div className="ad-form underline-box">
          <div id="ad-fields">
            <input type="text" className="inputborder" name="item" value={searchValues.item}
              onChange={(event)=>setSearchValues({...searchValues, item:event.target.value})} placeholder="Czego szukasz?" />
            <select name="category" className="inputborder" value={searchValues.category}
              onChange={(event)=>setSearchValues({...searchValues, category:event.target.value})}>
              <option value="">Wszystko</option>
              {categories&&categories.map(category=>(
                <option key={category.slug} value={category.slug}>{category.name}</option>
              ))}
            </select>
            <input type="number" className="inputborder" name="price" placeholder="cena max."
              value={searchValues.price} onChange={(event)=>setSearchValues({...searchValues, price:event.target.value})} />
          </div>
          <div>
            <Link to={`?${searchUrl}`}>
              <input type="button" id="ad-search" className="btn btn-primary" value="Szukaj"/>
            </Link>
          </div>
        </div>

        <div>
          <Link to="/ogloszenie/nowe" id="ad-add" style={{color:'white'}}>
            <div className="btn btn-primary" style={{textAlign:'center',marginBottom:'5px'}}>Dodaj</div>
          </Link>
        </div>

        {slug&&advertisement&&(
          <div id="ad-detail">
            <div className="buttons">
              <Link to={`/ogloszenie/${advertisement.slug}/edytuj`} className="edit btn btn-primary btn-mini">Edytuj</Link>&nbsp;
              <Link to="/ogloszenie/citroen-c4-cactus15/usun" className="delete btn btn-danger btn-mini" data-category="motoryzacja">Usun</Link>
            </div>

            <div className="underline-box">
              <h3>{advertisement.title}</h3>
              <div className="main-content">
                <ContentFormat text={advertisement.content} />
              </div>
              <div className="price-contact">
                <span>Kategoria: {advertisement.category}</span>
                <span style={{float:'right'}}>Cena: {advertisement.price} zł.</span>
              </div>
            </div>

          </div>
        )}

        <div className="table adv-table" id="ad-table">
          {data.results && data.results.map(advertisement => (
            <div key={advertisement.slug} className="row">
              <div><Link to={`/ogloszenie/${advertisement.slug}${history.location.search}`}>{advertisement.title}</Link></div>
              <div className="cell-price">{advertisement.price}</div>
            </div>
          ))}
        </div>
        <div id="paginator" style={{display:'table',width:'100%',fontSize:'30px',letterSpacing:'-4px'}} >
          <Link to={`?strona=${parseInt(currentPage)-1}${searchUrl&&`&${searchUrl}`}`} style={{float:'left',display:data.previous?'table':'none'}}>
            <div>
              <i className="fa fa-chevron-left" aria-hidden="true"></i>
              <i className="fa fa-chevron-left" aria-hidden="true"></i>
            </div>
          </Link>
          <Link to={`?strona=${parseInt(currentPage)+1}${searchUrl&&`&${searchUrl}`}`}
            style={{float:'right',cursor:'pointer',marginRight:'5px',display:data.next?'table':'none'}} onClick={()=>setCurrentPage(parseInt(currentPage)+1)}>
            <div>
              <i className="fa fa-chevron-right" aria-hidden="true"></i>
              <i className="fa fa-chevron-right" aria-hidden="true"></i>
            </div>
          </Link>
        </div>
      </div>
      {/*
          {JSON.stringify(data)}
        */}
    </Layout>
  )
}
export default Advertisements
