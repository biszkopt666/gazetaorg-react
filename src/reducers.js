export const initialState = {
  user: null,
  lastAdd: ['chuj','dupa','kurwa','cipa'],
  rightElements: []
}

export const reducer = (state, action) => {
  switch (action.type) {
  case 'USER':
    return {
      ...state,
      user: action.payload
    }
  case 'LOGOUT':
    return {
      ...state,
      user: null
    }
  case 'POSTS':
    // console.log("POSTS!")
    // console.log(action.payload)
    return {
      ...state,
      rightElements: action.payload
    }
  case 'CLEAR':
    return null
  default:
    return state
  }
}
