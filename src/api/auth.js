import axios from 'axios'

export const login = (email, password)  => {
  const body = JSON.stringify({ email, password })
  return axios.post('http://localhost:8000/api/accounts/login/', body, {
    headers: {
      'Content-Type': 'application/json',
    }
  })
    .then(res => {
      localStorage.setItem('token',JSON.stringify(res.data.token))
      return res
    })
    .catch(err => err.response)
}
export const userInfo = ()  => {
  return axios.get('http://localhost:8000/api/accounts/userinfo/', {
    headers: {
      'Content-Type': 'application/json',
      'Authorization':`Token ${JSON.parse(localStorage.getItem('token'))}`
    }
  })
    .then(res =>  res)
    .catch(err => err.response)
}
