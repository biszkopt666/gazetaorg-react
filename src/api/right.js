import axios from 'axios'

export const getList = async() => {
  const advertisements = await axios.get('http://localhost:8000/api/adios/?limit=40')
  const articles = await axios.get('http://localhost:8000/api/articles/?limit=40')
  const elements = []
  advertisements.data.results.forEach((item) => {
    item.slug = '/ogloszenie/'+item.slug
    elements.push(item)
  })
  articles.data.results.forEach((item) => {
    item.slug = '/artykul/'+item.slug
    elements.push(item)
  })
  elements.sort((a, b) => -a.created.localeCompare(b.created))
  // console.log('ADVY z API',elements)
  return elements
}
